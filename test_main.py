#coding=utf-8

'''
#1  方法一
    在console中，透過 --maxfail 避免遇到 fail 就停止
    其他方法，請見 https://stackoverflow.com/questions/36749796/how-to-run-all-pytest-tests-even-if-some-of-them-fail

    方法二 透過 delayed_assert
    assert 改成用 expect(表達式)
    需要使用 assert_expectations() 印出所有 expect() 的結果
    http://pythontesting.net/strategy/delayed-assert/
'''


import pytest
from delayed_assert import expect, assert_expectations
from main import *

# 驗證 add() 的正常用例
def test_add():
    assert add(1, 4) == 8   # fail
    assert add(1, 3) == 4   # pass
    assert add(2, 3) == 5   # pass

# 檢查程式是否結束
def test_raise_exit():
    with pytest.raises(SystemExit):
        raise_exit()

# 驗證 foo() 的正常用例
def test_foo_pass():
    assert foo(6) == 6

# 驗證 foo() 是否有引發自定義的錯誤
def test_foo_raise():
    with pytest.raises(MyError):
        foo(4)

# 驗證 foo() 的 Error message 是否正確
def test_foo_raise_message():
    with pytest.raises(MyError) as e:
        foo(3)
    assert ERROR1 in str(e)

# 檢查是否有某個屬性
def test_string():
    result = string("123")

    # 若引數是字串，會有 split 的屬性
    assert hasattr(result,"split")

    result = string(123)
    # 若引數是字串，不會有 split 的屬性
    assert not hasattr(result, "split")



